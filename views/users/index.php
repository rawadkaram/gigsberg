<?php


?>
<!-- views/users/index.php -->
<?php include  BASE_PATH. '/views/layouts/header.php'; ?>

<h2>Users List</h2>
<a type="button" href="/users/add" class="add-btn">Add User</a>
<table>
    <thead>
    <tr>
        <th>Username</th>
        <th>Email</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($users as $user): ?>
        <tr>
            <td class="username" data-id="<?= $user['id'] ?>"><?php echo htmlspecialchars($user['username']); ?></td>
            <td><?php echo htmlspecialchars($user['email']); ?></td>
            <td>
                <form method="post" action="/users/delete/<?php echo $user['id']; ?>"> <!-- Adjust the action URL as necessary -->
                    <button type="submit" class="delete-btn" onclick="return confirm('Are you sure?');">Delete</button>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<!-- Popup Modal for User Data -->
<div id="userModal" class="modal">
    <div class="modal-content">
        <span class="close-button">&times;</span>
        <div id="userData"><!-- User data will be displayed here --></div>
    </div>
</div>

<?php include BASE_PATH . '/views/layouts/footer.php'; ?>

