<?php
?>
<!-- views/users/add.php -->
<?php include BASE_PATH. '/views/layouts/header.php'; ?>

<h2>Add User</h2>
<form id="userForm" method="post" action="/users/add"> <!-- Adjust the action URL as necessary -->
    <label for="username">Username:</label>
    <input type="text" name="username" required id="username">
    <label for="email">Email:</label>
    <input type="email" name="email" required id="email">
    <label for="password">Password:</label>
    <input type="password" name="password" required id="password">
    <label for="birthdate">Birthdate:</label>
    <input type="date" name="birthdate" required id="birthdate">
    <label for="phone_number">Phone Number:</label>
    <input type="text" name="phone_number" required id="phone_number">
    <label for="url">URL:</label>
    <input type="url" name="url" required id="url">
    <button type="submit">Submit</button>
</form>

<?php include BASE_PATH. '/views/layouts/footer.php'; ?>

