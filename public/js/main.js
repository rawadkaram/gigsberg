$(document).ready(function(){
    $('.username').click(function(){
        var userId = $(this).data('id');
        $.ajax({
            url: '/users/getUserData/' + userId, // Adjust this URL based on your routing setup
            type: 'GET',
            success: function(response){
                var data = response;
                console.log(data);
                if (data.status === 'success') {
                    // Populate your modal with data.data
                    $('#userData').html(
                        '<p>ID: ' + data.data.id + '</p>' +
                        '<p>Username: ' + data.data.username + '</p>' +
                        '<p>Email: ' + data.data.email + '</p>' +
                        '<p>Password: ' + data.data.password + '</p>' +
                        '<p>Birthdate: ' + data.data.birthdate + '</p>' +
                        '<p>Phone: ' + data.data.phone_number + '</p>' +
                        '<p>URL: ' + data.data.url + '</p>'
                    );
                    $('#userModal').show();
                } else {
                    // Handle error
                    $('#userData').html('<p>Error: ' + data.message + '</p>');
                    $('#userModal').show();
                }
            }
        });
    });

    // When the user clicks on <span> (x), close the modal
    $('.close-button').click(function(){
        $('#userModal').hide();
    });

    // Optionally: close the modal if the user clicks anywhere outside of it
    $(window).click(function(event){
        if ($(event.target).is('#userModal')) {
            $('#userModal').hide();
        }
    });
});

document.getElementById('userForm').addEventListener('submit', function(event) {
    event.preventDefault(); // Prevent the form from submitting

    // Fetch form values
    var username = document.getElementById('username').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var birthdate = document.getElementById('birthdate').value;
    var phoneNumber = document.getElementById('phone_number').value;
    var url = document.getElementById('url').value;

    // Validation flags
    var isValid = true;
    var errorMessage = '';

    // Username validation: letters only
    if (!/^[a-zA-Z]+$/.test(username)) {
        isValid = false;
        errorMessage += 'Username must contain letters only.\n';
    }

    // Email validation
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
        isValid = false;
        errorMessage += 'Email is invalid.\n';
    }

    // Password validation: 8 chars min, 1 lowercase, 1 uppercase, 1 special character
    if (!/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W).{8,}/.test(password)) {
        isValid = false;
        errorMessage += 'Password must contain at least 8 characters, including 1 lowercase, 1 uppercase, and 1 special character.\n';
    }

    // Birthdate validation: not needed as input type="date" ensures valid format

    // Phone number validation: numbers only and 10 characters
    if (!/^\d{10}$/.test(phoneNumber)) {
        isValid = false;
        errorMessage += 'Phone number must be 10 digits.\n';
    }

    // URL validation
    if (!/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/.test(url)) {
        isValid = false;
        errorMessage += 'URL is invalid.\n';
    }

    // If all validations pass
    if (isValid) {
        $('#userForm').submit();
    } else {
        alert('Validation errors:\n' + errorMessage);
    }
});
