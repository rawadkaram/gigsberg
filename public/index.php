<?php
// public/index.php
require '../config/database.php';
require '../models/Users.php';
require '../controllers/UsersController.php';

// Initialize database connection

define('BASE_PATH', dirname(__FILE__). '/..' );

$db = new Database();
$userModel = new Users($db->getConnection());

// Instantiate the controller
$controller = new UsersController($userModel);


// Routing

$urlPath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$urlSegments = explode('/', trim($urlPath, '/'));
$page = $urlSegments[1] ?? null; // Get the page from the first segment
$id = $urlSegments[2] ?? null; // Get the id from the second segment if present

switch ($page) {
    case 'add':
        $controller->add();
        break;
    case 'delete':
        // Ensure $id is sanitized and validated before use
        if (!empty($id)) {
            $controller->delete($id);
        }
        break;
    case 'getUserData':
        // Ensure $id is sanitized and validated before use
        if (!empty($id)) {
            $controller->getUserData($id); // Ensure method name matches your implementation
        }
        break;
    default:
        $controller->index();
        break;
}

