<?php

class Users
{
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert($data)
    {
        // Prepare an INSERT statement with named placeholders
        $sql = "INSERT INTO users (username, email, password, birthdate, phone_number, url) VALUES (:username, :email, :password, :birthdate, :phone_number, :url)";

        try {
            $stmt = $this->db->prepare($sql);

            // Bind the data to the placeholders and execute the statement
            $stmt->bindParam(':username', $data['username']);
            $stmt->bindParam(':email', $data['email']);
            $stmt->bindParam(':password', $data['password']); // Consider hashing the password
            $stmt->bindParam(':birthdate', $data['birthdate']);
            $stmt->bindParam(':phone_number', $data['phone_number']);
            $stmt->bindParam(':url', $data['url']);

            $stmt->execute();
            return true;
        } catch (PDOException $e) {
            // Handle exception
            echo "Insertion error: " . $e->getMessage();
            return false;
        }
    }

    public function getAll()
    {
        $sql = "SELECT id, username, email, birthdate, phone_number, url FROM users";

        try {
            $stmt = $this->db->query($sql);
            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $users;
        } catch (PDOException $e) {
            // Handle exception
            echo "Error fetching users: " . $e->getMessage();
            return [];
        }
    }

    public function delete($id)
    {
        $sql = "DELETE FROM users WHERE id = :id";

        try {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return true;
        } catch (PDOException $e) {
            // Handle exception
            echo "Deletion error: " . $e->getMessage();
            return false;
        }
    }

    public function getUserById($id)
    {
        $sql = "SELECT id, username, email, birthdate, phone_number, url FROM users WHERE id = :id";
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            $user = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($user) {
                return $user;
            } else {
                // No user found with the provided ID
                return null;
            }
        } catch (PDOException $e) {
            // Handle exception
            echo "Error fetching user: " . $e->getMessage();
            return null;
        }
    }
}
