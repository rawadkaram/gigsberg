<?php
// controllers/UsersController.php
require_once '../models/Users.php'; // Adjust the path as necessary

class UsersController
{
    private $userModel;

    public function __construct($userModel)
    {
        $this->userModel = $userModel;
    }

    public function add()
    {
        // Check if the form has been submitted
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST data
            $postData = array_map('trim', $_POST);

            // Perform validation here (not shown for brevity)

            // If validation passes, insert the user
            $isInserted = $this->userModel->insert($postData);

            if ($isInserted) {
                // Redirect or show a success message
                header('Location: /users/index'); // Adjust the redirect location as necessary
            } else {
                // Handle insertion failure (e.g., show an error message)
            }
        } else {
            // Load the add user form view
            include '../views/users/create.php'; // Adjust the path as necessary
        }
    }

    public function index()
    {
        // Fetch all users
        $users = $this->userModel->getAll();

        // Load the view to display users
        include '../views/users/index.php'; // Adjust the path as necessary
    }

    public function delete($id)
    {
        // Call the model to delete the user by ID
        $isDeleted = $this->userModel->delete($id);

        if ($isDeleted) {
            // Redirect or show a success message
            header('Location: /users/index'); // Adjust the redirect location as necessary
        } else {
            // Handle deletion failure (e.g., show an error message)
        }
    }

    public function getUserData($id)
    {
        header('Content-Type: application/json');
        $userData = $this->userModel->getUserById($id);

        if ($userData) {
            // User found, return success response
            echo json_encode([
                'status' => 'success',
                'data' => $userData
            ]);
        } else {
            // User not found, return error response
            http_response_code(404); // Set HTTP response code to 404 (Not Found)
            echo json_encode([
                'status' => 'error',
                'message' => 'User not found'
            ]);
        }
        exit; // Prevent further execution
    }
}
